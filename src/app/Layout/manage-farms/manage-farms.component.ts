import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { InsertFarmComponent } from 'src/app/ModelPopup/insert-farm/insert-farm.component';
import { FarmserviceService } from 'src/app/Services/farmservice.service';

@Component({
  selector: 'app-manage-farms',
  templateUrl: './manage-farms.component.html',
  styleUrls: ['./manage-farms.component.css'],
})
export class ManageFarmsComponent implements OnInit {
  farms: any = [];
  localstorage: any;
  adminUser: any;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private farmservice: FarmserviceService,
    private loginregister: LoginregisterService,
    public matdialog: MatDialog,
    private alertService: AlertServiceService,
    private spinner: NgxSpinnerService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginregister
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    this.displayedColumns = [
      'edit',
      'delete',
      'Farm_name',
      'po_count',
      'po_trigger',
      'PO_Type',
      'supplier_group',
    ];
    this.getFarms();
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit(): void {}
  displayedColumns: string[] = [];
  dataSource: any = new MatTableDataSource(this.farms);

  getFarms() {
    this.spinner.show();
    this.farmservice.getAllFarmData().subscribe(
      (res: any) => {
        // console.log(res);
        if (res.status === 1) {
          this.farms = res.data;
          this.dataSource = new MatTableDataSource(this.farms);
          // console.log(this.dataSource);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
  deleteFarm(id: any) {
    confirm('Delete Account');
    this.farmservice.deleteFarm(id).subscribe(
      (res: any) => {
        if (res.status) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getFarms();
  }
  editFarm(farm: any) {
    let dialog = this.matdialog.open(InsertFarmComponent, {
      data: { farm },
    });
    dialog.afterClosed().subscribe(() => {
      this.getFarms();
    });
  }
  newFarm() {
    let dialog = this.matdialog.open(InsertFarmComponent);
    dialog.afterClosed().subscribe(() => {
      this.getFarms();
    });
  }
  replaceUnderscore(text: any) {
    if (text) {
      return text.replace(/_/g, ' ');
    }
  }
}
