import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  localstorage: any;
  adminUser: any;

  constructor(
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    private spinner: NgxSpinnerService,
    private loginregister: LoginregisterService,
    private router: Router
  ) {
    this.spinner.show();
    this.localstorage = sessionStorage.getItem('keys');
    this.loginregister
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    this.spinner.hide();
  }

  regFormData = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    status: ['active'],
    times_of_entries: [1],
  });

  ngOnInit(): void {}

  get f() {
    return this.regFormData.controls;
  }
  RegisterUser() {
    this.spinner.show();
    if (this.regFormData.valid) {
      this.loginregister.registerUser(this.regFormData.value).subscribe(
        (res: any) => {
          this.spinner.hide();
          if (res.success) {
            this.router.navigate(['/login']);
            this.alertService.onSuccess();
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.spinner.hide();
          this.alertService.onError(error.message);
        }
      );
    }
  }
}
