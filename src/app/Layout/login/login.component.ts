import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Idle } from '@ng-idle/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private loginservice: LoginregisterService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    private router: Router,
    private idle: Idle,
    private spinner: NgxSpinnerService
  ) {}

  loginFormData = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  ngOnInit(): void {}
  get f() {
    return this.loginFormData.controls;
  }

  onLogin() {
    this.spinner.show();
    if (this.loginFormData.valid) {
      let rawData = this.loginFormData.value;
      this.loginservice.loginUser(rawData).subscribe(
        (res: any) => {
          if (res.success) {
            sessionStorage.setItem('keys', JSON.stringify(res));
            this.router.navigate(['/home']);
            this.idle.watch();
            this.alertService.onSuccess();
          } else {
            this.alertService.onError(res.message);
          }
          this.spinner.hide();
        },
        (error: any) => {
          this.spinner.hide();
          this.alertService.onError(error.message);
        }
      );
    }
  }
}
