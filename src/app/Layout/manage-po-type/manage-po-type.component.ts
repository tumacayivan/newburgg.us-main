import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { InsertPoTypeComponent } from 'src/app/ModelPopup/insert-potype/insert-potype.component';
import { EditPoTypeComponent } from 'src/app/ModelPopup/edit-potype/edit-potype.component';
import { PoTypeserviceService } from 'src/app/Services/potypeservice.service';

@Component({
  selector: 'app-manage-po-type',
  templateUrl: './manage-po-type.component.html',
  styleUrls: ['./manage-po-type.component.css'],
})
export class ManagePoTypeComponent implements OnInit {
  potypes: any = [];
  localstorage: any;
  adminUser: any;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private potypeservice: PoTypeserviceService,
    private loginregister: LoginregisterService,
    public matdialog: MatDialog,
    private alertService: AlertServiceService,
    private spinner: NgxSpinnerService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginregister
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    this.displayedColumns = [
      'edit',
      'delete',
      'supplier_name',
      'farm_name',
      'minimum_po',
      'maximum_po',
      'po_type',
    ];
    this.getPoType();
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit(): void {}
  displayedColumns: string[] = [];
  dataSource: any = new MatTableDataSource(this.potypes);

  getPoType() {
    this.spinner.show();
    this.potypeservice.getAllPoTypeData().subscribe(
      (res: any) => {
        // console.log(res);
        if (res.status === 1) {
          this.potypes = res.data;
          this.dataSource = new MatTableDataSource(this.potypes);
          // console.log(this.dataSource);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
  deletePoType(farm_id: any, supplier_id: any) {
    confirm('Delete Account');
    this.potypeservice.deletePoType(supplier_id, farm_id).subscribe(
      (res: any) => {
        if (res.status) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getPoType();
  }
  editPoType(potype: any) {
    let dialog = this.matdialog.open(EditPoTypeComponent, {
      data: { potype },
    });
    dialog.afterClosed().subscribe(() => {
      this.getPoType();
    });
  }
  newPoType() {
    let dialog = this.matdialog.open(InsertPoTypeComponent);
    dialog.afterClosed().subscribe(() => {
      this.getPoType();
    });
  }
  replaceUnderscore(text: any) {
    if (text) {
      return text.replace(/_/g, ' ');
    }
  }
}
