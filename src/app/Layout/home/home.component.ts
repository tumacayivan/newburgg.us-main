import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { POComponent } from 'src/app/ModelPopup/po/po.component';
import { ReportInputComponent } from 'src/app/ModelPopup/report-input/report-input.component';
import { SettingDataValidationComponent } from 'src/app/ModelPopup/setting-data-validation/setting-data-validation.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {

  }

  openSettingDataValidation(){
    this.dialog.open(SettingDataValidationComponent);
  }

  openReportInsertDialog(){
    this.dialog.open(ReportInputComponent);
  }

  openPODialog(){
    this.dialog.open(POComponent);
  }
}
