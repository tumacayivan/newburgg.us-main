import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  constructor(
    private loginservice: LoginregisterService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  editFormData = this.fb.group({
    username: ['', Validators.required],
  });
  editEmailData = this.fb.group({
    email: ['', Validators.required],
  });
  passFormData = this.fb.group({
    oldpass: ['', Validators.required],
    newpass: ['', Validators.required],
  });
  ngOnInit(): void {}
  get f() {
    return this.editFormData.controls;
  }

  onEdit() {
    this.spinner.show();
    if (this.editFormData.valid) {
      let rawData = this.editFormData.value;
      var localstorage: any = sessionStorage.getItem('keys');
      rawData.email = JSON.parse(localstorage)['message'];
      this.loginservice.changeName(rawData).subscribe(
        (res: any) => {
          if (res.success) {
            let keys: any = JSON.parse(localstorage);
            keys.name = rawData.username;
            sessionStorage.setItem('keys', JSON.stringify(keys));
            this.router.navigate(['/home']);
            this.alertService.onSuccess();
          } else {
            this.alertService.onError(res.message);
          }
          this.spinner.hide();
        },
        (error: any) => {
          this.spinner.hide();
          this.alertService.onError(error.message);
        }
      );
    }
  }

  onEmailEdit() {
    this.spinner.show();
    if (this.editEmailData.valid) {
      let rawData = this.editEmailData.value;
      var localstorage: any = sessionStorage.getItem('keys');
      rawData.oldemail = JSON.parse(localstorage)['message'];
      this.loginservice.changeEmail(rawData).subscribe(
        (res: any) => {
          if (res.success) {
            let keys: any = JSON.parse(localstorage);
            keys.message = rawData.email;
            sessionStorage.setItem('keys', JSON.stringify(keys));
            this.router.navigate(['/home']);
            this.alertService.onSuccess();
          } else {
            this.alertService.onError(res.message);
          }
          this.spinner.hide();
        },
        (error: any) => {
          this.spinner.hide();
          this.alertService.onError(error.message);
        }
      );
    }
  }

  onPassChange() {
    this.spinner.show();
    let rawData = this.passFormData.value;
    var localstorage: any = sessionStorage.getItem('keys');
    rawData.email = JSON.parse(localstorage)['message'];
    this.loginservice.changePass(rawData).subscribe(
      (res: any) => {
        if (res.success) {
          this.router.navigate(['/home']);
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
}
