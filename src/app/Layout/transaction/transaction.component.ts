import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TransactionserviceService } from 'src/app/Services/transactionservice.service';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingServiceService } from 'src/app/Services/setting-service.service';
import { InsertPORowsComponent } from 'src/app/ModelPopup/insert-porows/insert-porows.component';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { WeightcalculatorComponent } from '../../ModelPopup/weightcalculator/weightcalculator.component';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { EditInvoiceComponent } from 'src/app/ModelPopup/edit-invoice/edit-invoice.component';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
})
export class TransactionComponent implements OnInit {
  constructor(
    public tservice: TransactionserviceService,
    public matdialog: MatDialog,
    private spinner: NgxSpinnerService,
    private settingService: SettingServiceService,
    private alertService: AlertServiceService,
    private loginservice: LoginregisterService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginuser = JSON.parse(this.localstorage)['name'] || 'admin';
    this.loginservice
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }

          if (this.adminUser) {
            this.displayedColumns = [
              'edit',
              'delete',
              'Supplier_Group',
              'Farm',
              'Newburg_Egg_PO',
              'Final_PO',
              'Type_Supplies',
              'Pick_Up_Date',
              'Farm_Order',
              'Qty 1',
              'Item 1',
              'Qty 2',
              'Item 2',
              'Qty 3',
              'Item 3',
              'Carrier',
              'Received_Date',
              'Received_Complete',
              'Notes',
              // 'Free_Form_Notes',
              // 'Final_Notes',
              'Count_Of_Damaged_Pallets',
              'Count_Of_Totes',
              'Shipment_Schedule',
              'Invoice_Number',
              'Invoice_Amount',
            ];
          } else {
            this.displayedColumns = [
              'edit',
              'Supplier_Group',
              'Farm',
              'Newburg_Egg_PO',
              'Final_PO',
              'Type_Supplies',
              'Pick_Up_Date',
              'Farm_Order',
              'Qty 1',
              'Item 1',
              'Qty 2',
              'Item 2',
              'Qty 3',
              'Item 3',
              'Carrier',
              'Received_Date',
              'Received_Complete',
              'Notes',
              // 'Free_Form_Notes',
              // 'Final_Notes',
              'Count_Of_Damaged_Pallets',
              'Count_Of_Totes',
              'Shipment_Schedule',
              'Invoice_Number',
              'Invoice_Amount',
            ];
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  localstorage: any;
  loginuser: any;
  adminUser: any;
  tableArr: any = [];
  supplierList: any = [];
  farmsList: any = [];
  displayedColumns: string[] = [];
  dataSource: any = new MatTableDataSource(this.tableArr);
  farmFilterList: any = [];
  supplierFilterList: any = [];
  search_word: any = '';

  ngOnInit(): void {
    this.allTransactionViewdata();
    this.allDropdownHeaderData();
  }

  allTransactionViewdata() {
    this.spinner.show();
    this.tservice.getAllTransactionData().subscribe(
      (res: any) => {
        if (res.data.length > 0) {
          const nos = res.data.filter((x: any) => x.Received_Complete === 'No');
          const blanks = res.data.filter(
            (x: any) =>
              x.Received_Complete == '' ||
              x.Received_Complete == null ||
              x.Received_Complete == 'null'
          );
          const others = res.data.filter(
            (x: any) => x.Received_Complete == 'Y'
          );
          this.tableArr = [...nos, ...blanks, ...others];
          for (let index = 0; index < this.tableArr.length; index++) {
            if (!this.tableArr[index].Pick_Up_Date) {
              continue;
            }
            this.tableArr[index].Pick_Up_Date =
              this.tableArr[index].Pick_Up_Date.split('T')[0];
            if (!this.tableArr[index].Received_Date) {
              continue;
            }
            this.tableArr[index].Received_Date =
              this.tableArr[index].Received_Date.split('T')[0];
          }
          this.dataSource = new MatTableDataSource(this.tableArr);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.applyFilter();
        this.spinner.hide();
      },
      (error) => {
        this.spinner.hide();
        console.log(error.message);
      }
    );
  }

  allDropdownHeaderData() {
    this.settingService.getSettindData().subscribe(
      (res: any) => {
        this.supplierList = res.data
          .filter((x: any) => x['setting_column'] === 'Supplier Group')
          .sort((a: any, b: any) =>
            a['setting_value'].localeCompare(b['setting_value'])
          );
        this.farmsList = res.data
          .filter((x: any) => x['setting_column'] === 'Farm')
          .sort((a: any, b: any) =>
            a['setting_value'].localeCompare(b['setting_value'])
          );
      },
      (error) => {
        this.alertService.onError(
          'something went wrong please try after some time'
        );
      }
    );
  }

  commonFilter() {
    var tableValues = this.tableArr;
    if (this.supplierFilterList.length > 0) {
      tableValues = tableValues.filter((x: any) =>
        this.supplierFilterList.includes(x['Supplier_Group'])
      );
    }
    if (this.farmFilterList.length > 0) {
      tableValues = tableValues.filter((x: any) =>
        this.farmFilterList.includes(x['Farm'])
      );
    }
    return tableValues;
  }

  removeFarmFilter(val: any) {
    for (var i = 0; i < this.farmFilterList.length; i++) {
      if (this.farmFilterList[i] === val) {
        this.farmFilterList.splice(i, 1);
        break;
      }
    }
    var tableValues = this.commonFilter();
    if (this.tableArr.length > 0) {
      this.dataSource = new MatTableDataSource(tableValues);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  removeSupplierFilter(val: any) {
    for (var i = 0; i < this.supplierFilterList.length; i++) {
      if (this.supplierFilterList[i] === val) {
        this.supplierFilterList.splice(i, 1);
        break;
      }
    }
    var tableValues = this.commonFilter();
    if (this.tableArr.length > 0) {
      this.dataSource = new MatTableDataSource(tableValues);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  filterByFarm(val: any) {
    if (!this.farmFilterList.includes(val)) {
      this.farmFilterList.push(val);
    }
    var tableValues = this.commonFilter();
    if (this.tableArr.length > 0) {
      this.dataSource = new MatTableDataSource(tableValues);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  filterBySupplier(val: any) {
    this.supplierFilterList.push(val);
    var tableValues = this.commonFilter();
    if (this.tableArr.length > 0) {
      this.dataSource = new MatTableDataSource(tableValues);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  applyFilter() {
    const filterValue = this.search_word;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editData(val: any) {
    let dialog = this.matdialog.open(InsertPORowsComponent, {
      data: { val },
    });
    dialog.afterClosed().subscribe(() => {
      this.allTransactionViewdata();
    });
  }

  editInvoice(val: any) {
    let dialog = this.matdialog.open(EditInvoiceComponent, {
      data: { val },
    });
    dialog.afterClosed().subscribe(() => {
      this.allTransactionViewdata();
    });
  }

  delete(val: any) {
    this.alertService.onConfirmation().then((x) => {
      this.spinner.show();
      if (x) {
        this.tservice.deleteTransaction(val).subscribe(
          (res: any) => {
            this.spinner.hide();
            this.alertService.onCustomSuccess(res.data);
            this.allTransactionViewdata();
          },
          (error: any) => {
            this.spinner.hide();
            this.alertService.onError(error.message);
          }
        );
      } else {
        this.spinner.hide();
      }
    });
  }

  public openDialog() {
    const dialogRef = this.matdialog.open(InsertPORowsComponent);
    dialogRef.afterClosed().subscribe(() => {
      this.allTransactionViewdata();
    });
  }

  public openWeightCalculator() {
    const weightCalc = this.matdialog.open(WeightcalculatorComponent);
    weightCalc.afterClosed().subscribe(() => {
      console.log('calculator is closed');
    });
  }

  Reset() {
    window.location.reload();
  }
}
