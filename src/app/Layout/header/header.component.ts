import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { AlertServiceService } from 'src/app/Services/alert-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  localstorage: any;
  loginuser: any;
  adminUser: any;

  constructor(
    private router: Router,
    private loginservice: LoginregisterService,
    private alertService: AlertServiceService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginuser = JSON.parse(this.localstorage)['name'] || 'admin';
    this.loginservice
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
  }

  ngOnInit(): void {}

  logout() {
    sessionStorage.removeItem('keys');
    this.router.navigate(['/login']);
  }
  edit() {
    this.router.navigate(['/edit']);
  }
  manageUser() {
    this.router.navigate(['/manage-user']);
  }
  manageSuppliers() {
    this.router.navigate(['/manage-suppliers']);
  }
  manageFarms() {
    this.router.navigate(['/manage-farms']);
  }
  managePoType() {
    this.router.navigate(['/manage-potype']);
  }
}
