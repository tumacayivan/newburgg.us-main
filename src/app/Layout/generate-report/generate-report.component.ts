import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { TransactionserviceService } from 'src/app/Services/transactionservice.service';

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css'],
})
export class GenerateReportComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private transactionSetvice: TransactionserviceService,
    private spinner: NgxSpinnerService,
    private alertServeive: AlertServiceService
  ) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  allDataArray: any = [];
  displayColumn: any = [];
  sGroupArray: any = [];
  sTypeArray: any = [];
  carrierArray: any = [];
  itemsArray: any = [];
  farmsArray: any = [];
  pickupdatefrom: any;
  pickupdateto: any;
  receivedfromDate: any;
  receivedToDate: any;

  totalQuantity: number = 0;

  ngOnInit(): void {
    this.spinner.show();
    this.getDataFromRoute();
    this.showTableData();
  }

  getDataFromRoute() {
    this.activatedRoute.queryParams.subscribe((x: any) => {
      this.allDataArray = JSON.parse(x.data);
      this.displayColumn = this.allDataArray.selectedColumns;
      this.sGroupArray = this.allDataArray.selectedSupplierGroup;
      this.sTypeArray = this.allDataArray.selectedSupplierType;
      this.carrierArray = this.allDataArray.selectedCarrier;
      this.itemsArray = this.allDataArray.selectedItems;
      this.farmsArray = this.allDataArray.selectedFarms;
      this.pickupdatefrom = this.allDataArray.pickupFromDate;
      this.pickupdateto = this.allDataArray.pickupTodate;
      this.receivedfromDate = this.allDataArray.receivedFromdate;
      this.receivedToDate = this.allDataArray.receivedTodate;
    });
  }

  allTransactionItem: any = [];
  displayedColumns: string[] = [];
  datasource = new MatTableDataSource(this.allTransactionItem);

  showTableData() {
    this.displayedColumns = this.displayColumn;
    this.transactionSetvice.getTransactionTable().subscribe(
      (res: any) => {
        if (this.displayedColumns.length > 0) {
          this.allTransactionItem = res.data.map((x: any) => {
            return this.displayedColumns.reduce(function (a: any, b: any) {
              if (b == 'Items') {
                a[b] = x['Description'];
              } else {
                a[b] = x[b];
              }
              return a;
            }, {});
          });
          console.log(`display col calls : ${this.allTransactionItem.length}`);
        }

        if (this.pickupdatefrom && this.pickupdateto) {
          this.allTransactionItem = this.allTransactionItem.filter(
            (x: any) =>
              new Date(this.pickupdatefrom).getTime() <=
                new Date(x['Pick_Up_Date']).getTime() &&
              new Date(x['Pick_Up_Date']).getTime() <=
                new Date(this.pickupdateto).getTime()
          );
          console.log(
            `display from pickup date :${this.allTransactionItem.length}`
          );
        }

        if (this.receivedfromDate && this.receivedToDate) {
          this.allTransactionItem = this.allTransactionItem.filter(
            (x: any) =>
              new Date(this.receivedfromDate).getTime() <=
                new Date(x['Received_Date']).getTime() &&
              new Date(x['Received_Date']).getTime() <=
                new Date(this.receivedToDate).getTime()
          );
          console.log(
            `display from received date :${this.allTransactionItem.length}`
          );
        }

        if (this.sGroupArray.length > 0) {
          this.allTransactionItem = this.allTransactionItem.filter((x: any) =>
            this.sGroupArray.includes(x['Supplier_Group'])
          );
          console.log(`supplier group : ${this.allTransactionItem.length}`);
        }

        if (this.sTypeArray.length > 0) {
          this.allTransactionItem = this.allTransactionItem.filter((x: any) =>
            this.sTypeArray.includes(x['Type_Supplies'])
          );
          console.log(`supplier type : ${this.allTransactionItem.length}`);
        }

        if (this.itemsArray.length > 0) {
          this.allTransactionItem = this.allTransactionItem.filter((x: any) =>
            this.itemsArray.includes(x['Items'])
          );
          console.log(`items : ${this.allTransactionItem.length}`);
        }
        if (this.farmsArray.length > 0) {
          this.allTransactionItem = this.allTransactionItem.filter((x: any) =>
            this.farmsArray.includes(x['Farm'])
          );
          console.log(`farms array : ${this.allTransactionItem.length}`);
        }
        if (this.carrierArray.length > 0) {
          this.allTransactionItem = this.allTransactionItem.filter((x: any) =>
            this.carrierArray.includes(x['Carrier'])
          );
          console.log(`carrier array : ${this.allTransactionItem.length}`);
        }

        this.allTransactionItem.map((x: any) => {
          this.totalQuantity += x['Quantity'];
        });

        this.datasource = new MatTableDataSource(this.allTransactionItem);
        this.datasource.paginator = this.paginator;
        this.spinner.hide();
      },
      (err: any) => {
        this.spinner.hide();
        console.log(err.message);
        this.alertServeive.onError(
          'Something went wrong. Please try after some time.'
        );
      }
    );
  }
}
