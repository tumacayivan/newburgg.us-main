import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralPOModelComponent } from 'src/app/ModelPopup/general-pomodel/general-pomodel.component';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoservicesService } from 'src/app/Services/poservices.service';

@Component({
  selector: 'app-general-po',
  templateUrl: './general-po.component.html',
  styleUrls: ['./general-po.component.css']
})
export class GeneralPOComponent implements OnInit {

  constructor(
    private alertService: AlertServiceService,
    private poservice: PoservicesService,
    private matDialog: MatDialog,
    private spinner: NgxSpinnerService,
  ) { }
  value = '';

  displayedColumns: string[] = ['po', 'farm', 'action'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator!: MatPaginator

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData(){
    this.spinner.show();
    this.poservice.getAllData().subscribe(
      (res: any) => {
        // console.log(res.message);
        this.dataSource = new MatTableDataSource(res.message);
        this.dataSource.paginator = this.paginator;
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    )
  }

  openGeneralPODialog(){
   let dialog = this.matDialog.open(GeneralPOModelComponent);
   dialog.afterClosed().subscribe(
     (res: any)=>{
       this.getAllData();
     }
   )
  }

  editData(val:any){
   let dialog = this.matDialog.open(GeneralPOModelComponent, {
      data: val
    })
    dialog.afterClosed().subscribe(
      (res: any)=>{
        this.getAllData();
      }
    )
  }

  deleteData(id: any){
    this.alertService.onConfirmation().then((x)=>{
      if(x){
        this.poservice.deleteDataGeneralPO(id).subscribe(
          (res: any)=>{
            if(res.success){
              this.alertService.onCustomSuccess('data has been deleted.');
              this.getAllData();
            }
          },
          (error: any)=>{
            this.alertService.onError(error.message);
          }
        )
      }
    })
  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
