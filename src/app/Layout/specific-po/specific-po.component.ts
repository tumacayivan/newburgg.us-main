import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FirstComponent } from 'src/app/ModelPopup/specialPopup/first/first.component';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoservicesService } from 'src/app/Services/poservices.service';

@Component({
  selector: 'app-specific-po',
  templateUrl: './specific-po.component.html',
  styleUrls: ['./specific-po.component.css']
})
export class SpecificPOComponent implements OnInit {

  constructor(
    private poservice: PoservicesService,
    private dialog: MatDialog,
    private alertService: AlertServiceService
  ) { }

  allData: any = [];
  Header: any;
  displayedColumns: any[] = ['inputValue', 'action'];
  dataSource = new MatTableDataSource(this.allData);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.poservice.getSpecialPOData().subscribe(
      (res: any) => {
        this.allData = res.data;
        // console.log(this.allData);
      },
      (error: any) => {
        this.alertService.onError(error.message);
      }
    )
  }

  openDialog() {
    let dialog = this.dialog.open(FirstComponent);
    dialog.afterClosed().subscribe(
      (res: any) => {
        this.poservice.getSpecialPOData().subscribe(
          (res: any) => {
            this.dataSource = res.data.filter((x: any) => x['farm'] === this.Header)
          }
        )
      }
    )
  }


  getFilterData(val: any) {
    this.Header = val;
    let filterItem = this.allData.filter((x: any) => x['farm'] === val);
    this.dataSource = new MatTableDataSource(filterItem);
  }

  editData(val: any) {
    let dialog = this.dialog.open(FirstComponent, {
      data: val
    });

    dialog.afterClosed().subscribe(
      (res: any) => {
        this.poservice.getSpecialPOData().subscribe(
          (res: any) => {
            this.dataSource = res.data.filter((x: any) => x['farm'] === this.Header);
          }
        )
      }
    );
  }

  deleteData(id: number) {
    this.alertService.onConfirmation().then((x) => {
      if (x) {
        this.poservice.deleteData(id).subscribe(
          (res: any) => {
            if (res.success) {
              this.alertService.onCustomSuccess('data has been deleted');
              this.poservice.getSpecialPOData().subscribe(
                (res: any) => {
                  this.dataSource = res.data.filter((x: any) => x['farm'] === this.Header);
                }
              )
            } else {
              this.alertService.onError(res.message);
            }
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
      }
    })
  }

}
