import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { InsertSupplierComponent } from 'src/app/ModelPopup/insert-supplier/insert-supplier.component';
import { SupplierserviceService } from 'src/app/Services/supplierservice.service';

@Component({
  selector: 'app-manage-suppliers',
  templateUrl: './manage-suppliers.component.html',
  styleUrls: ['./manage-suppliers.component.css'],
})
export class ManageSuppliersComponent implements OnInit {
  suppliers: any = [];
  localstorage: any;
  adminUser: any;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private supplierservice: SupplierserviceService,
    private loginregister: LoginregisterService,
    public matdialog: MatDialog,
    private alertService: AlertServiceService,
    private spinner: NgxSpinnerService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginregister
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    this.displayedColumns = [
      'edit',
      'delete',
      'supplier_name',
      'po_count',
      'po_trigger',
      'po_type',
      'farm',
    ];
    this.getSuppliers();
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit(): void {}
  displayedColumns: string[] = [];
  dataSource: any = new MatTableDataSource(this.suppliers);

  getSuppliers() {
    this.spinner.show();
    this.supplierservice.getAllSupplierData().subscribe(
      (res: any) => {
        // console.log(res);
        if (res.status === 1) {
          this.suppliers = res.data;
          this.dataSource = new MatTableDataSource(this.suppliers);
          // console.log(this.dataSource);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
  deleteSupplier(id: any) {
    confirm('Delete Account');
    this.supplierservice.deleteSupplier(id).subscribe(
      (res: any) => {
        if (res.status) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getSuppliers();
  }
  editSupplier(supplier: any) {
    let dialog = this.matdialog.open(InsertSupplierComponent, {
      data: { supplier },
    });
    dialog.afterClosed().subscribe(() => {
      this.getSuppliers();
    });
  }
  newSupplier() {
    let dialog = this.matdialog.open(InsertSupplierComponent);
    dialog.afterClosed().subscribe(() => {
      this.getSuppliers();
    });
  }
  replaceUnderscore(text: any) {
    if (text) {
      return text.replace(/_/g, ' ');
    }
  }
}
