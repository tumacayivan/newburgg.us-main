import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { InsertUserComponent } from 'src/app/ModelPopup/insert-user/insert-user.component';
import { UsersService } from 'src/app/Services/users.service';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css'],
})
export class ManageUserComponent implements OnInit {
  users: any = [];
  localstorage: any;
  adminUser: any;

  constructor(
    private router: Router,
    public matdialog: MatDialog,

    private userservice: UsersService,
    private loginregister: LoginregisterService,
    private alertService: AlertServiceService,
    private spinner: NgxSpinnerService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginregister
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe(
        (res: any) => {
          if (res.success) {
            if (res.admin == 'Y') {
              this.adminUser = true;
            } else {
              this.adminUser = false;
            }
          } else {
            this.alertService.onError(res.message);
          }
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    this.displayedColumns = [
      'delete',
      'username',
      'email',
      'admin_account',
      'toggle_admin',
    ];

    this.getUsers();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = [];
  dataSource: any = new MatTableDataSource(this.users);

  ngOnInit(): void {}
  getUsers() {
    this.spinner.show();
    this.users = this.userservice.getAllUsers().subscribe(
      (res: any) => {
        if (res.success) {
          this.users = res.result;
          this.dataSource = new MatTableDataSource(this.users);
          // console.log(this.dataSource);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
  register() {
    let dialog = this.matdialog.open(InsertUserComponent);
    dialog.afterClosed().subscribe(() => {
      this.getUsers();
    });
  }
  removeAdmin(email: any) {
    confirm('Remove Admin');
    this.userservice.removeAdmin(email).subscribe(
      (res: any) => {
        if (res.success) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getUsers();
  }
  makeAdmin(email: any) {
    confirm('Make Admin');
    this.userservice.makeAdmin(email).subscribe(
      (res: any) => {
        if (res.success) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getUsers();
  }
  deleteAccount(email: any) {
    confirm('Delete Account');
    this.userservice.deletAccount(email).subscribe(
      (res: any) => {
        if (res.success) {
          this.alertService.onSuccess();
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.getUsers();
  }
}
