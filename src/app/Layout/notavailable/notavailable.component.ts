import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notavailable',
  templateUrl: './notavailable.component.html',
  styleUrls: ['./notavailable.component.css']
})
export class NotavailableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
