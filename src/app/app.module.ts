import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './Layout/home/home.component';
import { TransactionComponent } from './Layout/transaction/transaction.component';
import { LoginComponent } from './Layout/login/login.component';
import { RegistrationComponent } from './Layout/registration/registration.component';
import { NotavailableComponent } from './Layout/notavailable/notavailable.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './Material/material/material.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './Layout/header/header.component';
import { InsertPORowsComponent } from './ModelPopup/insert-porows/insert-porows.component';
import { InsertSupplierComponent } from './ModelPopup/insert-supplier/insert-supplier.component';
import { InsertPoTypeComponent } from './ModelPopup/insert-potype/insert-potype.component';
import { EditPoTypeComponent } from './ModelPopup/edit-potype/edit-potype.component';
import { InsertUserComponent } from './ModelPopup/insert-user/insert-user.component';
import { InsertFarmComponent } from './ModelPopup/insert-farm/insert-farm.component';
import { ReportInputComponent } from './ModelPopup/report-input/report-input.component';
import { GenerateReportComponent } from './Layout/generate-report/generate-report.component';
import { SettingDataValidationComponent } from './ModelPopup/setting-data-validation/setting-data-validation.component';
import { AddSettingComponent } from './ModelPopup/add-setting/add-setting.component';
import { IntercepterService } from './Services/intercepter.service';
import { POComponent } from './ModelPopup/po/po.component';
import { GeneralPOComponent } from './Layout/general-po/general-po.component';
import { SpecificPOComponent } from './Layout/specific-po/specific-po.component';
import { GeneralPOModelComponent } from './ModelPopup/general-pomodel/general-pomodel.component';
import { FirstComponent } from './ModelPopup/specialPopup/first/first.component';
import { WeightcalculatorComponent } from './ModelPopup/weightcalculator/weightcalculator.component';
import { EditComponent } from './Layout/edit/edit.component';
import { ManageUserComponent } from './Layout/manage-user/manage-user.component';
import { ManageSuppliersComponent } from './Layout/manage-suppliers/manage-suppliers.component';
import { ManageFarmsComponent } from './Layout/manage-farms/manage-farms.component';
import { ManagePoTypeComponent } from './Layout/manage-po-type/manage-po-type.component';
import { EditInvoiceComponent } from './ModelPopup/edit-invoice/edit-invoice.component';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TransactionComponent,
    LoginComponent,
    EditComponent,
    ManageUserComponent,
    ManageSuppliersComponent,
    ManageFarmsComponent,
    ManagePoTypeComponent,
    RegistrationComponent,
    NotavailableComponent,
    HeaderComponent,
    InsertPORowsComponent,
    InsertUserComponent,
    InsertSupplierComponent,
    InsertFarmComponent,
    InsertPoTypeComponent,
    EditPoTypeComponent,
    EditInvoiceComponent,
    ReportInputComponent,
    GenerateReportComponent,
    SettingDataValidationComponent,
    AddSettingComponent,
    POComponent,
    GeneralPOComponent,
    SpecificPOComponent,
    GeneralPOModelComponent,
    FirstComponent,
    WeightcalculatorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    HttpClientModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IntercepterService,
      multi: true,
    },
    BsModalService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
