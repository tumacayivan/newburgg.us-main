import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';

@Component({
  selector: 'app-insert-user',
  templateUrl: './insert-user.component.html',
  styleUrls: ['./insert-user.component.css'],
})
export class InsertUserComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InsertUserComponent>,
    private loginregister: LoginregisterService,

    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  formdata = this.fb.group({
    username: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    admin: ['False', Validators.required],
  });
  adminArray: any = ['False', 'True'];

  ngOnInit(): void {}

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;

      this.loginregister.registerUser(rawData).subscribe(
        (_res: any) => {
          this.alertService.onCustomSuccess(_res.data);
          this.dialogRef.close();
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
