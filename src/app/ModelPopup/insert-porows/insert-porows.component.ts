import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { SettingServiceService } from 'src/app/Services/setting-service.service';
import { TransactionserviceService } from 'src/app/Services/transactionservice.service';
import { WeightcalculatorComponent } from '../weightcalculator/weightcalculator.component';

@Component({
  selector: 'app-insert-porows',
  templateUrl: './insert-porows.component.html',
  styleUrls: ['./insert-porows.component.css'],
})
export class InsertPORowsComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InsertPORowsComponent>,
    private tservice: SettingServiceService,
    private transactionServeice: TransactionserviceService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  transactionID: any;
  lineID: any;

  formdata = this.fb.group({
    Supplier_Group: ['', Validators.required],
    Farm: ['', Validators.required],
    Newburg_Egg_PO: [''],
    Final_PO: [''],
    Type_Supplies: [''],
    Pick_Up_Date: [''],
    Farm_Order: [''],
    Carrier: [''],
    Received_Date: [''],
    'Qty 1': [''],
    'Item 1': [''],
    'Qty 2': [''],
    'Item 2': [''],
    'Qty 3': [''],
    'Item 3': [''],
    header_id: [''],
    line_id: [''],
    Notes: [''],
    Free_Form_Notes: [''],
    Received_Complete: [''],
    Count_Of_Damaged_Pallets: [''],
    Count_Of_Totes: [''],
    PO_Count: [''],
    Shipment_Schedule: [''],
  });

  allSettingData: any = [];
  suppliersArray: any = [];
  formArray: any = [];
  quantityArray: any = [];
  itmsArray: any = [];
  carrerArray: any = [];
  typeSupplier: any = [];
  notesData: any = [];
  freeFormNotes: any = [];
  notes: any = [];

  ngOnInit(): void {
    this.transactionServeice.lastInsertedData().subscribe((res: any) => {
      this.transactionID = res.data[0].transaction_id;
      this.lineID = res.data[0].line_id;
    });
    this.getAllDropdownValues();
  }

  formatPickUpDate = () => {
    if (this.data.val.Pick_Up_Date) {
      var convertDate = new Date(this.data.val.Pick_Up_Date);
      convertDate.setDate(convertDate.getDate() + 1);

      this.data.val.Pick_Up_Date = convertDate;
    }
    if (this.data.val.Received_Date) {
      var convertDate2 = new Date(this.data.val.Received_Date);
      convertDate2.setDate(convertDate2.getDate() + 1);
      console.log(convertDate2);
      this.data.val.Received_Date = convertDate2;
    }
  };

  formatDateAfterSubmission = (rawData: any) => {
    if (rawData.Pick_Up_Date) {
      if (!this.PickDateChange) {
        var convertDate = new Date(rawData.Pick_Up_Date);
        convertDate.setDate(convertDate.getDate() - 1);

        rawData.Pick_Up_Date = convertDate;
      }
    }
    if (rawData.Received_Date) {
      if (!this.ReceivedDateChange) {
        var convertDate2 = new Date(rawData.Received_Date);
        convertDate2.setDate(convertDate2.getDate() - 1);
        console.log(convertDate2);
        rawData.Received_Date = convertDate2;
      }
    }
    return rawData;
  };

  PickDateChange: boolean = false;
  ReceivedDateChange: boolean = false;

  pickDateChanged = () => {
    this.PickDateChange = true;
  };

  receivedDateChanged = () => {
    this.ReceivedDateChange = true;
  };

  public patchValues = () => {
    this.formdata.patchValue({
      Supplier_Group: [this.data.val.Supplier_Group],
      Farm: [this.data.val.Farm],
      Newburg_Egg_PO: this.data.val.Newburg_Egg_PO,
      Final_PO: this.data.val.Final_PO,
      Pick_Up_Date: this.data.val.Pick_Up_Date,
      Farm_Order: this.data.val.Farm_Order,
      Carrier: this.data.val.Carrier,
      'Qty 1': `${this.data.val['Qty 1']}`,
      'Item 1': this.data.val['Item 1'],
      'Qty 2': `${this.data.val['Qty 2']}`,
      'Item 2': this.data.val['Item 2'],
      'Qty 3': `${this.data.val['Qty 3']}`,
      'Item 3': this.data.val['Item 3'],
      Received_Date: this.data.val.Received_Date,
      Notes: this.notes,
      Free_Form_Notes: this.freeFormNotes,
      Type_Supplies: [this.data.val.Type_Supplies],
      Received_Complete: this.data.val.Received_Complete,
      Count_Of_Damaged_Pallets: this.data.val.Count_Of_Damaged_Pallets,
      Count_Of_Totes: this.data.val.Count_Of_Totes,
      PO_Count: this.data.val.PO_Count,
      Shipment_Schedule: this.data.val.Shipment_Schedule,
    });
  };

  getAllDropdownValues() {
    this.tservice.getSettindData().subscribe(
      (res: any) => {
        this.allSettingData = res.data;
        this.suppliersArray = res.data.filter(
          (x: any) => x['setting_column'] === 'Supplier Group'
        );
        this.suppliersArray.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });
        this.quantityArray = res.data.filter(
          (x: any) => x['setting_column'] === 'QTY'
        );
        this.quantityArray.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(Number(b.setting_value), 'en', {
            numeric: true,
          });
        });
        this.itmsArray = res.data.filter(
          (x: any) => x['setting_column'] === 'Item'
        );
        this.itmsArray.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });
        this.carrerArray = res.data.filter(
          (x: any) => x['setting_column'] === 'Carrier'
        );
        this.carrerArray.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });
        this.notesData = res.data.filter(
          (x: any) => x['setting_column'] === 'Notes'
        );
        this.notesData.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });

        this.typeSupplier = res.data.filter(
          (x: any) => x['setting_column'] === 'Supply'
        );
        this.typeSupplier.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });

        if (this.data) {
          console.log(this.data);
          //to select form value load this first
          let setting_value = this.data.val.Supplier_Group;
          let setting_id = this.allSettingData.filter(
            (x: any) => x['setting_value'] == setting_value
          )[0]['setting_id'];
          this.formArray = this.allSettingData.filter(
            (x: any) => x['header_id'] === setting_id
          );
          this.formArray.sort(function (a: any, b: any) {
            return a.setting_value.localeCompare(b.setting_value);
          });
          this.formatPickUpDate();

          var getNotes = () => {
            var notes = this.data.val.Notes.split(',');
            for (let i = 0; i < notes.length; i++) {
              const x = notes[i];
              var exist = false;
              this.notesData.map((y: any) => {
                if (y.setting_value === x.trim()) {
                  exist = true;
                }
              });
              if (exist) {
                this.notes.push(x.trim());
              } else {
                this.freeFormNotes += x.trim();
              }
            }
          };
          if (this.data.val.Notes != null) {
            this.notes = [];
            getNotes();
          } else {
            this.notes = '';
          }
          console.log(this.notes);
          this.patchValues();
        }
      },
      (error) => {
        this.alertService.onError(error.message);
      }
    );
  }

  selectItems(value: any) {
    let selectedId = value['setting_id'];
    this.allSettingData.filter((x: any) => {
      this.formArray = this.allSettingData.filter(
        (x: any) => x['header_id'] === selectedId
      ); //235
      this.formArray.sort(function (a: any, b: any) {
        return a.setting_value.localeCompare(b.setting_value);
      });
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      console.log('RawData');
      console.log(rawData);
      rawData.Supplier_Group = rawData.Supplier_Group[0];
      rawData.Farm = rawData.Farm[0];
      rawData.Notes = rawData.Notes != '' ? rawData.Notes.join(',') : '';
      rawData.Notes += ', ' + rawData.Free_Form_Notes;
      rawData.header_id = Number(this.transactionID) + 1;
      rawData.line_id = Number(1);
      if (rawData.Type_Supplies.length > 0) {
        rawData.Type_Supplies = rawData.Type_Supplies[0];
      } else {
        rawData.Type_Supplies = null;
      }
      if (this.data) {
        console.log(this.data);
        console.log(`edit data`);
        rawData.transaction_id = this.data.val.transaction_id;
        rawData.header_id = this.data.val.header_id;
        if (!this.PickDateChange || !this.ReceivedDateChange) {
          rawData = this.formatDateAfterSubmission(rawData);
        }
        var newRawData = rawData;
        if (rawData['Qty 1'] || rawData['Qty 2'] || rawData['Qty 3']) {
          if (rawData['Qty 1'] || rawData['Item 1']) {
            newRawData['Qty'] = rawData['Qty 1'];
            newRawData['Item'] = rawData['Item 1'];
            newRawData.line_id = 1;
            this.transactionServeice
              .updateTransactionData(newRawData)
              .subscribe(
                (_res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
          }
          if (rawData['Qty 2'] || rawData['Item 2']) {
            newRawData['Qty'] = rawData['Qty 2'];
            newRawData['Item'] = rawData['Item 2'];
            newRawData.line_id = 2;
            this.transactionServeice
              .updateTransactionData(newRawData)
              .subscribe(
                (res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
          }
          if (rawData['Qty 3'] || rawData['Item 3']) {
            newRawData['Qty'] = rawData['Qty 3'];
            newRawData['Item'] = rawData['Item 3'];
            newRawData.line_id = 3;
            this.transactionServeice
              .updateTransactionData(newRawData)
              .subscribe(
                (res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
          }
        } else {
          newRawData.line_id = null;
          newRawData['Qty'] = null;
          newRawData['Item'] = null;
          this.transactionServeice.updateTransactionData(newRawData).subscribe(
            (res: any) => {
              // this.alertService.onCustomSuccess(res.data);
              // this.dialogRef.close();
            },
            (error: any) => {
              this.alertService.onError(error.message);
            }
          );
        }
        this.alertService.onSuccess();
        this.dialogRef.close();
      } else {
        console.log(`insert new record`);
        if (!rawData.PO_Count) {
          rawData.PO_Count = 1;
        }
        for (let i = 0; i < rawData.PO_Count; i++) {
          var newRawData = rawData;
          if (rawData['Qty 1'] || rawData['Qty 2'] || rawData['Qty 3']) {
            if (rawData['Qty 1'] || rawData['Item 1']) {
              newRawData['Qty'] = rawData['Qty 1'];
              newRawData['Item'] = rawData['Item 1'];
              newRawData.line_id = 1;
              this.transactionServeice.insertDataInPoRows(newRawData).subscribe(
                (_res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
            }
            if (rawData['Qty 2'] || rawData['Item 2']) {
              newRawData['Qty'] = rawData['Qty 2'];
              newRawData['Item'] = rawData['Item 2'];
              newRawData.line_id = 2;
              this.transactionServeice.insertDataInPoRows(newRawData).subscribe(
                (res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
            }
            if (rawData['Qty 3'] || rawData['Item 3']) {
              newRawData['Qty'] = rawData['Qty 3'];
              newRawData['Item'] = rawData['Item 3'];
              newRawData.line_id = 3;
              this.transactionServeice.insertDataInPoRows(newRawData).subscribe(
                (res: any) => {
                  // this.alertService.onCustomSuccess(res.data);
                  // this.dialogRef.close();
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              );
            }
          } else {
            newRawData.line_id = null;
            newRawData['Qty'] = null;
            newRawData['Item'] = null;
            this.transactionServeice.insertDataInPoRows(newRawData).subscribe(
              (res: any) => {
                // this.alertService.onCustomSuccess(res.data);
                // this.dialogRef.close();
              },
              (error: any) => {
                this.alertService.onError(error.message);
              }
            );
          }
          rawData.header_id = Number(rawData.header_id) + 1;
        }

        this.alertService.onSuccess();
        this.dialogRef.close();
      }
    } else {
      this.alertService.onError('Please enter required value');
    }
  }

  public openWeightCalculator() {
    const weightCalc = this.matDialog.open(WeightcalculatorComponent);
    weightCalc.afterClosed().subscribe(() => {
      console.log('calculator is closed');
    });
  }
}
