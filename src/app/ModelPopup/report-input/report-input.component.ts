import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSelectionList } from '@angular/material/list';
import { NavigationExtras, Router } from '@angular/router';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { SettingServiceService } from 'src/app/Services/setting-service.service';

@Component({
  selector: 'app-report-input',
  templateUrl: './report-input.component.html',
  styleUrls: ['./report-input.component.css'],
})
export class ReportInputComponent implements OnInit {
  constructor(
    private settingService: SettingServiceService,
    public fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<ReportInputComponent>,
    private router: Router,
    private alertService: AlertServiceService
  ) {}

  formData = this.fb.group({
    selectedColumns: [''],
    selectedSupplierGroup: [''],
    selectedSupplierType: [''],
    selectedItems: [''],
    selectedFarms: [''],
    selectedCarrier: [''],
    pickupFromDate: [''],
    pickupTodate: [''],
    receivedFromdate: [''],
    receivedTodate: [''],
  });

  allSettingData: any = [];
  supplyList: any = [];
  supplyTypes: any = [];
  items: any = [];
  farms: any = [];
  carrier: any = [];
  allColumns: any = [
    'Supplier_Group',
    'Farm',
    'Newburg_Egg_PO',
    'Type_Supplies',
    'Pick_Up_Date',
    'Farm_Order',
    'Items',
    'Quantity',
    'Carrier',
    'Received_Date',
    'Final_PO',
  ];

  ngOnInit(): void {
    this.allDropdowndata();
  }

  allDropdowndata() {
    this.settingService.getSettindData().subscribe(
      (res: any) => {
        this.allSettingData = res.data;
        this.supplyList = this.allSettingData.filter(
          (x: any) => x['setting_column'] === 'Supplier Group'
        );
        this.supplyTypes = res.data.filter(
          (x: any) => x['setting_column'] === 'Supply'
        );
        this.items = res.data.filter(
          (x: any) => x['setting_column'] === 'Item'
        );
        this.farms = res.data.filter(
          (x: any) => x['setting_column'] === 'Farm'
        );
        this.carrier = res.data.filter(
          (x: any) => x['setting_column'] === 'Carrier'
        );
      },
      (error: any) => {
        console.log(error.message);
      }
    );
  }
  @ViewChild('allSelected', { static: true })
  public allSelected!: MatSelectionList;
  @ViewChild('allSupplyGroup', { static: true })
  public allSupplyGroup!: MatSelectionList;
  @ViewChild('farmList', { static: true }) public farmList!: MatSelectionList;
  @ViewChild('supplierType', { static: true })
  public supplierType!: MatSelectionList;
  @ViewChild('carrierType', { static: true })
  public carrierType!: MatSelectionList;
  @ViewChild('itemsType', { static: true }) public itemsType!: MatSelectionList;

  selectAll(val: boolean) {
    if (val) {
      this.allSelected.selectAll();
    } else {
      this.allSelected.deselectAll();
    }
  }

  supplyGroup(val: boolean) {
    if (val) {
      this.allSupplyGroup.selectAll();
    } else {
      this.allSupplyGroup.deselectAll();
    }
  }

  formGroup(val: boolean) {
    if (val) {
      this.farmList.selectAll();
    } else {
      this.farmList.deselectAll();
    }
  }

  SupplierTypeGroup(val: boolean) {
    if (val) {
      this.supplierType.selectAll();
    } else {
      this.supplierType.deselectAll();
    }
  }

  carrierTypeGroup(val: boolean) {
    if (val) {
      this.carrierType.selectAll();
    } else {
      this.carrierType.deselectAll();
    }
  }

  itemTypeGroup(val: boolean) {
    if (val) {
      this.itemsType.selectAll();
    } else {
      this.itemsType.deselectAll();
    }
  }
  onsubmit() {
    let rawData = this.formData.value;
    if (rawData.selectedColumns.length > 0) {
      this.router.navigate(['/genReport'], {
        queryParams: { data: JSON.stringify(rawData) },
      });
      this.dialogRef.close();
    } else {
      this.alertService.onError('pleaes select colum and other details');
    }
  }
  closedDialog() {
    this.dialogRef.close();
  }
}
