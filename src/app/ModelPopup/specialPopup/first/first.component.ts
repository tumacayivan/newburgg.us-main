import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoservicesService } from 'src/app/Services/poservices.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(
    private fb: UntypedFormBuilder,
    private poservice: PoservicesService,
    private alertService: AlertServiceService,
    private dialogRef: MatDialogRef<FirstComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  formData = this.fb.group({
    inputValue: ['', Validators.required],
    farm: ['', Validators.required]
  })

  ngOnInit(): void {
    this.formData.patchValue(this.data);
  }
  farms: any[] = [
    { value: 'avrumy', viewValue: 'Avrumy' },
    { value: 'pedro', viewValue: 'Pedro' },
    { value: 'weaver', viewValue: 'Weaver' },
    { value: 'gettysburg', viewValue: 'Gettysburg' },
    { value: 'eci', viewValue: 'ECI' },
    { value: 'sj', viewValue: 'S&J' },
    { value: 'rc', viewValue: 'R/C' },
    { value: 'house1', viewValue: 'House 1' },
    { value: 'house2', viewValue: 'House 2' },
    { value: 'wenning', viewValue: 'Wenning' },
    { value: 'esbenshade', viewValue: 'Esbenshade' }
  ];


  onSubmit() {
    if (this.data) {
      if (this.formData.valid) {
        let rawdata = this.formData.value;
        rawdata.spo_id = this.data.spo_id;
        this.poservice.updateData(rawdata).subscribe(
          (res: any) => {
            if (res.success) {
              this.alertService.onSuccess();
              this.dialogRef.close();
            } else {
              this.alertService.onError(res.message);
            }
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        )
      }
    } else {
      if (this.formData.valid) {
        let { inputValue, farm } = this.formData.value;

        this.poservice.getSpecialPOData().subscribe(
          (res: any) => {
            let lastInputValue = res.data.filter((x: any) => x['farm'] == farm).slice(-1)[0]['inputValue'];
            let finalData = [];

            for (let i = 1; i <= inputValue; i++) {
              finalData.push([Number(lastInputValue) + i, farm]);
            }
            if (finalData.length > 0) {
              this.poservice.insertData(finalData).subscribe(
                (res: any) => {
                  // console.log(res);
                  if (res.success) {
                    this.alertService.onSuccess();
                    this.dialogRef.close();
                  }
                },
                (error: any) => {
                  this.alertService.onError(error.message);
                }
              )
            }
          }
        )
      }
    }

  }
}
