import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { SupplierserviceService } from 'src/app/Services/supplierservice.service';
import { FarmserviceService } from 'src/app/Services/farmservice.service';

@Component({
  selector: 'app-insert-porows',
  templateUrl: './insert-supplier.component.html',
  styleUrls: ['./insert-supplier.component.css'],
})
export class InsertSupplierComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InsertSupplierComponent>,
    private supplierServeice: SupplierserviceService,
    private farmService: FarmserviceService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  formdata = this.fb.group({
    supplier_name: ['', Validators.required],
    po_count: [''],
    po_trigger: [''],
    po_type: [''],
    farm: [''],
  });
  poTypeArray: any = ['', 'Newburg_Egg_PO', 'Farm'];
  farmsArray: any = [];

  ngOnInit(): void {
    this.farmService.getAllFarmData().subscribe(
      (res: any) => {
        if (res.status === 1) {
          this.farmsArray = res.data.map((x: any) => x.Farm_name);
        } else {
          this.alertService.onError(res.message);
        }
      },
      (error: any) => {
        this.alertService.onError(error.message);
      }
    );
    if (this.data) {
      this.patchValues();
    }
  }

  public patchValues = () => {
    console.log(this.data.supplier);
    this.formdata.patchValue({
      supplier_name: this.data.supplier.supplier_name,
      po_count: this.data.supplier.po_count,
      po_trigger: this.data.supplier.po_trigger,
      po_type: this.data.supplier.po_type,
      farm: this.data.supplier.farm,
    });
  };

  // selectItems(value: any) {
  //   let selectedValue = value;
  //   this.allSettingData.filter((x: any) => {
  //     this.formArray = this.allSettingData.filter(
  //       (x: any) => x['header_id'] === selectedId
  //     ); //235
  //     this.formArray.sort(function (a: any, b: any) {
  //       return a.setting_value.localeCompare(b.setting_value);
  //     });
  //   });
  // }

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      console.log('RawData');
      console.log(rawData);
      if (this.data) {
        console.log(this.data);
        console.log(`edit data`);
        rawData.supplier_id = this.data.supplier.supplier_id;
        this.supplierServeice.updateSupplierData(rawData).subscribe(
          (_res: any) => {
            this.alertService.onCustomSuccess(_res.data);
            this.dialogRef.close();
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
        this.alertService.onSuccess();
        this.dialogRef.close();
      } else {
        console.log(`insert new record`);
        this.supplierServeice.insertData(rawData).subscribe(
          (_res: any) => {
            this.alertService.onCustomSuccess(_res.data);
            this.dialogRef.close();
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
      }
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
