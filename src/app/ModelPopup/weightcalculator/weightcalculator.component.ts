import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-weightcalculator',
  templateUrl: './weightcalculator.component.html',
  styleUrls: ['./weightcalculator.component.css'],
})
export class WeightcalculatorComponent implements OnInit {
  constructor(private matDialog: MatDialogRef<WeightcalculatorComponent>) {}
  weight!: number;
  pallets!: number;
  average!: any;

  ngOnInit(): void {}
  closed() {
    this.matDialog.close();
  }

  getUnit(val: any) {
    if (val <= 1100) {
      return '36-38';
    } else if (val < 1200 && val >= 1100) {
      return '38 - 40';
    } else if (val < 1300 && val >= 1200) {
      return '40 - 42';
    } else if (val < 1400 && val >= 1300) {
      return '42 - 44';
    } else if (val < 1500 && val >= 1400) {
      return '44 - 46';
    } else if (val < 1600 && val >= 1500) {
      return '46 - 48';
    } else if (val < 1700 && val >= 1600) {
      return '48 - 50';
    } else if (val < 1800 && val >= 1700) {
      return '50 - 52';
    } else {
      return '50 - 52';
    }
  }
}
