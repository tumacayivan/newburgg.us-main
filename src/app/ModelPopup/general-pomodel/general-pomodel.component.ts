import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoservicesService } from 'src/app/Services/poservices.service';

@Component({
  selector: 'app-general-pomodel',
  templateUrl: './general-pomodel.component.html',
  styleUrls: ['./general-pomodel.component.css']
})
export class GeneralPOModelComponent implements OnInit {
  constructor(
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    private poservice: PoservicesService,
    public dialogRef: MatDialogRef<GeneralPOModelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  formData = this.fb.group({
    po: ['', Validators.required],
    farm: ['', Validators.required]
  })

  ngOnInit(): void {
    this.formData.patchValue(this.data);
    this.getAllDropDownData();
  }

  allValues: any[] = [];
  getAllDropDownData() {
    this.poservice.getAllData().subscribe(
      (res: any) => {
        this.allValues = [...new Set(res.message.map((x: any) => x['farm']))]
      }
    )
  }

  onSubmit() {
    if (this.data) {
      if (this.formData.valid) {
        let rawdata = this.formData.value;
        rawdata.g_id = this.data.g_id;
        this.poservice.updateDataInGeneralPO(rawdata).subscribe(
          (res: any) => {
            if (res.success) {
              // console.log(res);
              this.alertService.onCustomSuccess('data has been updated.');
              this.dialogRef.close();
            }
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        )
      }
    } else {
      if (this.formData.valid) {
        let { po, farm } = this.formData.value;

        this.poservice.getAllData().subscribe(
          (res: any) => {
            let lastData = res.message.filter((x: any) => x['farm'] === farm).slice(-1)[0]['po'];
            let finalData = [];

            for (let i = 1; i <= po; i++) {
              finalData.push([Number(lastData) + i, farm])
            }

            if (finalData.length > 0) {
              this.poservice.insertDataInGeneralPO(finalData).subscribe(
                (res: any) => {
                  if (res.success) {
                    this.alertService.onSuccess();
                    this.dialogRef.close();
                  }
                },
                (error: any) => {
                  this.alertService.onError(error.message)
                }
              )
            }
          }
        )
      }
    }
  }
}
