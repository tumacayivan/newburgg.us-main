import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { SettingServiceService } from 'src/app/Services/setting-service.service';
import { TransactionserviceService } from 'src/app/Services/transactionservice.service';
import { WeightcalculatorComponent } from '../weightcalculator/weightcalculator.component';

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.css'],
})
export class EditInvoiceComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<EditInvoiceComponent>,
    private tservice: SettingServiceService,
    private transactionServeice: TransactionserviceService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  transactionID: any;
  lineID: any;

  formdata = this.fb.group({
    Invoice_Number: [''],
    Invoice_Amount: [''],
    Carrier: [''],
  });

  allSettingData: any = [];
  carrerArray: any = [];

  ngOnInit(): void {
    this.transactionServeice.lastInsertedData().subscribe((res: any) => {
      this.transactionID = res.data[0].transaction_id;
      this.lineID = res.data[0].line_id;
    });
    this.getAllDropdownValues();
    if (this.data) {
      this.patchValues();
    }
  }

  public patchValues = () => {
    this.formdata.patchValue({
      Invoice_Number: this.data.val.Invoice_Number,
      Invoice_Amount: this.data.val.Invoice_Amount,
      Carrier: this.data.val.Carrier,
    });
  };

  getAllDropdownValues() {
    this.tservice.getSettindData().subscribe(
      (res: any) => {
        this.allSettingData = res.data;
        this.carrerArray = res.data.filter(
          (x: any) => x['setting_column'] === 'Carrier'
        );
        this.carrerArray.sort(function (a: any, b: any) {
          return a.setting_value.localeCompare(b.setting_value);
        });
      },
      (error) => {
        this.alertService.onError(error.message);
      }
    );
  }

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      if (this.data) {
        rawData.transaction_id = this.data.val.transaction_id;
        rawData.header_id = this.data.val.header_id;
        this.transactionServeice.updateInvoiceData(rawData).subscribe(
          (res: any) => {
            console.log(res);
            if (res.status) {
              this.alertService.onCustomSuccess(res.data);
            } else {
              this.alertService.onWarning(res.data);
            }
            this.dialogRef.close();
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
      }
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
