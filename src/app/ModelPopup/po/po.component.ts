import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-po',
  templateUrl: './po.component.html',
  styleUrls: ['./po.component.css']
})
export class POComponent implements OnInit {

  constructor(
    private matdialog: MatDialogRef<POComponent>,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.matdialog.close();
  }
  gotogeneralpo() {
    this.router.navigate(['/generalpo']);
    this.matdialog.close();
  }
  gotospecificpo() {
    this.router.navigate(['/specificpo']);
    this.matdialog.close();
  }
}
