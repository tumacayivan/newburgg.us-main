import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoTypeserviceService } from 'src/app/Services/potypeservice.service';
import { SupplierserviceService } from 'src/app/Services/supplierservice.service';

@Component({
  selector: 'app-insert-porows',
  templateUrl: './edit-potype.component.html',
  styleUrls: ['./edit-potype.component.css'],
})
export class EditPoTypeComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<EditPoTypeComponent>,
    private potypeserveice: PoTypeserviceService,
    private supplierService: SupplierserviceService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  formdata = this.fb.group({
    minimum_po: ['', Validators.required],
    maximum_po: ['', Validators.required],
    po_type: ['', Validators.required],
  });
  poTypeArray: any = ['Farm_Order', 'Newburg_Egg_PO'];
  poArray: any = [];
  suppliersArray: any = [];
  ngOnInit(): void {
    if (this.data) {
      this.patchValues();
    }
  }

  public patchValues = () => {
    console.log(this.data.potype.po_type);
    this.formdata.patchValue({
      maximum_po: this.data.potype.maximum_po,
      minimum_po: this.data.potype.minimum_po,
      po_type: this.data.potype.po_type,
    });
  };

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      rawData.farm_id = this.data.potype.farm_id;
      rawData.supplier_id = this.data.potype.supplier_id;
      console.log(rawData);
      rawData.potype_id = this.data.potype.potype_id;
      this.potypeserveice.updatePoTypeData(rawData).subscribe(
        (_res: any) => {
          this.alertService.onCustomSuccess(_res.data);
          this.dialogRef.close();
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
      this.alertService.onSuccess();
      this.dialogRef.close();
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
