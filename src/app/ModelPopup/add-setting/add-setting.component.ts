import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { SettingServiceService } from 'src/app/Services/setting-service.service';

@Component({
  selector: 'app-add-setting',
  templateUrl: './add-setting.component.html',
  styleUrls: ['./add-setting.component.css']
})
export class AddSettingComponent implements OnInit {
  
  constructor(
    public dref: MatDialogRef<AddSettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: UntypedFormBuilder,
    private settingService: SettingServiceService,
    private alertService: AlertServiceService
  ) { }

  settingFormData = this.fb.group({
    setting_column:[this.data[0]],
    setting_value: ['', Validators.required],
    header_id: [''],
    setting_id: ['']
  })

  SupplierGroup: any=[];

  ngOnInit(): void {
    this.settingFormData.patchValue(this.data);
    this.settingService.getSettindData().subscribe(
      (res: any)=> {
        this.SupplierGroup = res.data.filter((x: any)=> x["setting_column"] ==='Supplier Group');
      }
    )
  }

  onsubmit(){
    let rawdata = this.settingFormData.value;
    if(rawdata.setting_id){
      this.settingService.updateData(rawdata).subscribe(
        (res: any) => {
          this.dref.close();
          this.alertService.onSuccess();
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      )
    } else{
      this.settingService.insertData(rawdata).subscribe(
        (res: any) =>{
          this.dref.close();
          this.alertService.onSuccess();
        },
        (error: any) =>{
          this.alertService.onError('Please try after some time.')
        }
      )
     }
  }

  closeDialog(){
    this.dref.close();
  }
}
