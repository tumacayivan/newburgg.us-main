import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { FarmserviceService } from 'src/app/Services/farmservice.service';
import { SupplierserviceService } from 'src/app/Services/supplierservice.service';

@Component({
  selector: 'app-insert-porows',
  templateUrl: './insert-farm.component.html',
  styleUrls: ['./insert-farm.component.css'],
})
export class InsertFarmComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InsertFarmComponent>,
    private farmServeice: FarmserviceService,
    private supplierService: SupplierserviceService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  formdata = this.fb.group({
    Farm_name: ['', Validators.required],
    po_count: [''],
    po_trigger: [''],
    PO_Type: [''],
    supplier_group: ['', Validators.required],
  });
  poTypeArray: any = ['', 'Newburg_Egg_PO'];
  suppliersArray: any = [];

  ngOnInit(): void {
    this.supplierService.getAllSupplierData().subscribe(
      (res: any) => {
        if (res.status === 1) {
          this.suppliersArray = res.data.map((x: any) => x.supplier_name);
        } else {
          this.alertService.onError(res.message);
        }
      },
      (error: any) => {
        this.alertService.onError(error.message);
      }
    );
    if (this.data) {
      this.patchValues();
    }
  }

  public patchValues = () => {
    console.log(this.data.farm);
    this.formdata.patchValue({
      Farm_name: this.data.farm.Farm_name,
      po_count: this.data.farm.po_count,
      po_trigger: this.data.farm.po_trigger,
      PO_Type: this.data.farm.PO_Type,
      supplier_group: this.data.farm.supplier_group,
    });
  };

  // selectItems(value: any) {
  //   let selectedValue = value;
  //   this.allSettingData.filter((x: any) => {
  //     this.formArray = this.allSettingData.filter(
  //       (x: any) => x['header_id'] === selectedId
  //     ); //235
  //     this.formArray.sort(function (a: any, b: any) {
  //       return a.setting_value.localeCompare(b.setting_value);
  //     });
  //   });
  // }

  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      console.log('RawData');
      console.log(rawData);
      if (this.data) {
        console.log(this.data);
        console.log(`edit data`);
        rawData.farm_id = this.data.farm.farm_id;
        this.farmServeice.updateFarmData(rawData).subscribe(
          (_res: any) => {
            this.alertService.onCustomSuccess(_res.data);
            this.dialogRef.close();
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
        this.alertService.onSuccess();
        this.dialogRef.close();
      } else {
        console.log(`insert new record`);
        this.farmServeice.insertData(rawData).subscribe(
          (_res: any) => {
            this.alertService.onCustomSuccess(_res.data);
            this.dialogRef.close();
          },
          (error: any) => {
            this.alertService.onError(error.message);
          }
        );
      }
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
