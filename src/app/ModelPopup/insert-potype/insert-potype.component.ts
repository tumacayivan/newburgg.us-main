import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { PoTypeserviceService } from 'src/app/Services/potypeservice.service';
import { SupplierserviceService } from 'src/app/Services/supplierservice.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-insert-porows',
  templateUrl: './insert-potype.component.html',
  styleUrls: ['./insert-potype.component.css'],
})
export class InsertPoTypeComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InsertPoTypeComponent>,
    private potypeserveice: PoTypeserviceService,
    private spinner: NgxSpinnerService,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public matDialog: MatDialog
  ) {}

  formdata = this.fb.group({
    supplier_name: ['', Validators.required],
    farm_name: ['none', Validators.required],
    minimum_po: ['', Validators.required],
    maximum_po: ['', Validators.required],
    po_type: ['Newburg_Egg_PO', Validators.required],
  });
  poTypeArray: any = ['Farm_Order', 'Newburg_Egg_PO'];
  supplierArray: any = [];
  supplierData: any = [];
  farmData: any = [];
  farmArray: any = ['none'];
  ngOnInit(): void {
    this.potypeserveice.getFarms().subscribe(
      (res: any) => {
        if (res.status === 1) {
          this.farmData = res.data;
          res.data.map((x: any) => {
            this.farmArray.push(x.setting_value);
          });
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
    this.potypeserveice.getSuppliers().subscribe(
      (res: any) => {
        if (res.status === 1) {
          this.supplierData = res.data;
          res.data.map((x: any) => {
            this.supplierArray.push(x.setting_value);
          });
        } else {
          this.alertService.onError(res.message);
        }
        this.spinner.hide();
      },
      (error: any) => {
        this.spinner.hide();
        this.alertService.onError(error.message);
      }
    );
  }
  closeDialog() {
    this.dialogRef.close();
  }
  onSubmit() {
    if (this.formdata.valid) {
      let rawData = this.formdata.value;
      this.supplierData.map((x: any) => {
        if (x.setting_value === rawData.supplier_name) {
          rawData.supplier_id = x.setting_id;
        }
      });
      this.farmData.map((x: any) => {
        if (rawData.farm_name === 'none') {
          rawData.farm_id = 'null';
        } else {
          if (x.setting_value === rawData.supplier_name) {
            rawData.farm_id = x.setting_id;
          }
        }
      });

      this.potypeserveice.insertData(rawData).subscribe(
        (_res: any) => {
          this.alertService.onCustomSuccess(_res.data);
          this.dialogRef.close();
        },
        (error: any) => {
          this.alertService.onError(error.message);
        }
      );
    } else {
      this.alertService.onError('Please enter required value');
    }
  }
}
