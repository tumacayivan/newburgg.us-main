import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AlertServiceService } from 'src/app/Services/alert-service.service';
import { LoginregisterService } from 'src/app/Services/loginregister.service';
import { SettingServiceService } from 'src/app/Services/setting-service.service';
import { AddSettingComponent } from '../add-setting/add-setting.component';

@Component({
  selector: 'app-setting-data-validation',
  templateUrl: './setting-data-validation.component.html',
  styleUrls: ['./setting-data-validation.component.css'],
})
export class SettingDataValidationComponent implements OnInit {
  constructor(
    private settingService: SettingServiceService,
    private dialog: MatDialog,
    private fb: UntypedFormBuilder,
    private alertService: AlertServiceService,
    private loginservice: LoginregisterService
  ) {
    this.localstorage = sessionStorage.getItem('keys');
    this.loginuser = JSON.parse(this.localstorage)['name'] || 'admin';
    this.loginservice
      .isAdmin({ user: JSON.parse(this.localstorage)['message'] })
      .subscribe((res: any) => {
        if (res.success) {
          if (res.admin == 'Y') {
            this.adminUser = true;
          } else {
            this.adminUser = false;
          }
        } else {
          this.alertService.onError(res.message);
        }
      });
  }
  localstorage: any;
  loginuser: any;
  adminUser: any;
  allSettingData: any = [];
  suppliersArray: any = [];
  allrelativeData: any = [];

  formData = this.fb.group({
    setting_col: [''],
    setting_val: [''],
  });

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.settingService.getSettindData().subscribe(
      (res: any) => {
        this.allSettingData = res.data;
        this.suppliersArray = [
          ...new Set(res.data.map((x: any) => x['setting_column'])),
        ];
      },
      (error) => {
        console.log(error);
      }
    );
  }

  temdata: any = [];
  selectData(val: any) {
    let selectedValue = val[0];
    this.temdata = selectedValue;
    this.settingService.getSettindData().subscribe((res: any) => {
      this.allrelativeData = [
        ...res.data.filter((x: any) => x['setting_column'] === selectedValue),
      ];
    });
  }

  onAddClick() {
    let leftVal = this.formData.value.setting_col;
    if (leftVal.length > 0) {
      let dialogRef = this.dialog.open(AddSettingComponent, {
        data: leftVal,
      });
      dialogRef.afterClosed().subscribe((res: any) => {
        this.selectData([this.temdata]);
      });
    } else {
      this.alertService.onWarning(
        'Please select a left side setting to update.'
      );
    }
  }

  editData() {
    let resData = this.formData.value;
    if (resData['setting_val'][0]) {
      let dr = this.dialog.open(AddSettingComponent, {
        data: resData['setting_val'][0],
      });

      dr.afterClosed().subscribe((result: any) => {
        this.selectData([this.temdata]);
      });
    } else {
      this.alertService.onWarning('please select item to update');
    }
  }

  deleteValue() {
    if (this.formData.value['setting_val']) {
      let getvalue = this.formData.value.setting_val[0]['setting_id'];
      this.alertService.onConfirmation().then((x: any) => {
        if (x) {
          this.settingService.deletedata(getvalue).subscribe(
            (res: any) => {
              this.selectData([this.temdata]);
              this.alertService.onCustomSuccess(res.data);
            },
            (error: any) => {
              this.alertService.onError(error.message);
            }
          );
        }
      });
    } else {
      this.alertService.onError('Please select item to delete');
    }
  }

  closeMainDialog() {
    this.dialog.closeAll();
  }
}
