import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardGuard } from './guard.guard';
import { EditComponent } from './Layout/edit/edit.component';
import { GeneralPOComponent } from './Layout/general-po/general-po.component';
import { GenerateReportComponent } from './Layout/generate-report/generate-report.component';
import { HomeComponent } from './Layout/home/home.component';
import { LoginComponent } from './Layout/login/login.component';
import { ManageUserComponent } from './Layout/manage-user/manage-user.component';
import { NotavailableComponent } from './Layout/notavailable/notavailable.component';
import { RegistrationComponent } from './Layout/registration/registration.component';
import { ManageSuppliersComponent } from './Layout/manage-suppliers/manage-suppliers.component';
import { ManageFarmsComponent } from './Layout/manage-farms/manage-farms.component';
import { ManagePoTypeComponent } from './Layout/manage-po-type/manage-po-type.component';
import { SpecificPOComponent } from './Layout/specific-po/specific-po.component';
import { TransactionComponent } from './Layout/transaction/transaction.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'edit', component: EditComponent, canActivate: [GuardGuard] },
  {
    path: 'manage-user',
    component: ManageUserComponent,
    canActivate: [GuardGuard],
  },
  {
    path: 'manage-suppliers',
    component: ManageSuppliersComponent,
    canActivate: [GuardGuard],
  },
  {
    path: 'manage-farms',
    component: ManageFarmsComponent,
    canActivate: [GuardGuard],
  },
  {
    path: 'manage-potype',
    component: ManagePoTypeComponent,
    canActivate: [GuardGuard],
  },
  {
    path: 'register',
    component: RegistrationComponent,
    canActivate: [GuardGuard],
  },
  { path: 'home', component: HomeComponent, canActivate: [GuardGuard] },
  {
    path: 'transaction',
    component: TransactionComponent,
    canActivate: [GuardGuard],
  },
  {
    path: 'genReport',
    component: GenerateReportComponent,
    canActivate: [GuardGuard],
  },
  // { path: 'generalpo', component: GeneralPOComponent },
  // { path: 'specificpo', component: SpecificPOComponent },
  { path: '**', component: NotavailableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
