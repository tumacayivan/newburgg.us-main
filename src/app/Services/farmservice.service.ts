import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FarmserviceService {
  url = `${environment.apiBaseUrl}`;
  constructor(private http: HttpClient) {}

  getAllFarmData() {
    //view_transaction
    return this.http.get(`${this.url}/farm/all`);
  }
  deleteFarm(name: any) {
    //delet Transactions data
    return this.http.delete(`${this.url}/farm/delete/${name}`);
  }
  insertData(data: any) {
    return this.http.post(`${this.url}/farm/post`, data);
  }
  updateFarmData(data: any) {
    return this.http.put(`${this.url}/farm/update`, data);
  }
}
