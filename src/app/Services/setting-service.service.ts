import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SettingServiceService {
  url = `${environment.apiBaseUrl}`;
  constructor(private http: HttpClient) {}

  getSettindData() {
    return this.http.get(`${this.url}/setting/all`);
  }

  insertData(data: any) {
    return this.http.post(`${this.url}/setting/post`, data);
  }

  updateData(data: any) {
    return this.http.put(`${this.url}/setting/update`, data);
  }

  deletedata(id: number) {
    return this.http.delete(`${this.url}/setting/delete/${id}`);
  }
}
