import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PoservicesService {
  url: any = `${environment.apiBaseUrl}`;
  constructor(
    private http: HttpClient
  ) { }
  
  //===============================general po section============================
  getAllData() {
    return this.http.get(`${this.url}/gnpo/all`);
  }

  insertDataInGeneralPO(data: any) {
    return this.http.post(`${this.url}/gnpo/post`, data);
  }

  updateDataInGeneralPO(data: any) {
    return this.http.put(`${this.url}/gnpo/update`, data);
  }

  deleteDataGeneralPO(id: number) {
    return this.http.delete(`${this.url}/gnpo/delete/${id}`);
  }
  //============================special PO section=======================================

  getSpecialPOData(){
    return this.http.get(`${this.url}/newsppo`);
  }

  insertData(data: any) {
    return this.http.post(`${this.url}/newsppo/post`, data);
  }

  updateData(data: any) {
    return this.http.put(`${this.url}/newsppo/update`, data);
  }

  deleteData(id: number) {
    return this.http.delete(`${this.url}/newsppo/delete/${id}`);
  }
}
