import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SupplierserviceService {
  url = `${environment.apiBaseUrl}`;
  constructor(private http: HttpClient) {}

  getAllSupplierData() {
    //view_transaction
    return this.http.get(`${this.url}/supplier/all`);
  }
  deleteSupplier(name: any) {
    //delet Transactions data
    return this.http.delete(`${this.url}/supplier/delete/${name}`);
  }
  insertData(data: any) {
    return this.http.post(`${this.url}/supplier/post`, data);
  }
  updateSupplierData(data: any) {
    return this.http.put(`${this.url}/supplier/update`, data);
  }
}
