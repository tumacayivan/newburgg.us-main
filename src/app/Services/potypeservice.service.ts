import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PoTypeserviceService {
  url = `${environment.apiBaseUrl}`;
  constructor(private http: HttpClient) {}
  getSuppliers() {
    //view_transaction
    return this.http.get(`${this.url}/potype/get-suppliers`);
  }
  getFarms() {
    //view_transaction
    return this.http.get(`${this.url}/potype/get-farms`);
  }
  getAllPoTypeData() {
    //view_transaction
    return this.http.get(`${this.url}/potype/all`);
  }
  deletePoType(supplier_id: any, farm_id: any) {
    //delet Transactions data
    return this.http.delete(
      `${this.url}/potype/delete/${supplier_id}/${farm_id}`
    );
  }
  insertData(data: any) {
    return this.http.post(`${this.url}/potype/post`, data);
  }
  updatePoTypeData(data: any) {
    return this.http.put(`${this.url}/potype/update`, data);
  }
}
