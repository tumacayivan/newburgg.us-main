import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TransactionserviceService {
  url = `${environment.apiBaseUrl}`;
  constructor(private http: HttpClient) {}

  getAllTransactionData() {
    //view_transaction
    return this.http.get(`${this.url}/transaction/all`);
  }
  getTransactionTable() {
    //otiginal table
    return this.http.get(`${this.url}/transaction/tn`);
  }

  deleteTransaction(id: number) {
    //delet Transactions data
    return this.http.delete(`${this.url}/transaction/delete/${id}`);
  }

  lastInsertedData() {
    return this.http.get(`${this.url}/transaction/lastdata`);
  }

  insertDataInPoRows(data: any) {
    return this.http.post(`${this.url}/transaction/post`, data);
  }
  updateTransactionData(data: any) {
    return this.http.put(`${this.url}/transaction/update`, data);
  }
  updateInvoiceData(data: any) {
    return this.http.put(`${this.url}/transaction/invoice/update`, data);
  }
}
